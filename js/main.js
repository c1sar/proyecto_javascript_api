var key = "vfGEpINgYzmsh8MH8byebSVWvkOOp1617RcjsnL6pGolvzIV6u";
var list = [];
var prevPageToken = '';
var nextPageToken = '';
var query = '';

function showList(list){
	$('#result').empty();
	var delay = 500;
	for (var i = 0; i < list.length; i++) {
		var video = list[i];
		$('#video-title').text(video.title);
		$('#video-description').text(video.description);
		$('#video-img').attr('src', video.img.medium);
		link = 'https://www.youtube.com/watch?v=' + video.videoId;
		$('#video-link').attr('href', link);
		$('#video-link-title').attr('href', link);

		var videoElement = $('#panel').children().clone();

		$('#result').append(videoElement);
		$('#result #video-title').prop('id', '');
		$('#result #video-description').prop('id', '');
		$('#result #video-img').prop('id', '');
		$('#result #video-link').prop('id', '');

		videoElement.hide();
		videoElement.delay(delay * i).fadeIn();

	}
}

function getVideo(query, token) {

	var ignJson = {
        query: query,
        maxResults: 10,
        pageToken: token
    };


	$.ajax({
        url: "https://zazkov-youtube-grabber-v1.p.mashape.com/search.video.php",
        method: 'GET',
        dataType: 'JSON',
        data: ignJson,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-Mashape-Key', key)
        }
    })
    .done(function (response) {
        prevPageToken = response["control"]["prevPageToken"];
        nextPageToken = response["control"]["nextPageToken"];

        if(prevPageToken === null || prevPageToken === '') 
        	$('#prev').attr('disabled', true);
        else
        	$('#prev').attr('disabled', false);

        if(nextPageToken === null || nextPageToken === '') 
        	$('#next').attr('disabled', true);
        else
        	$('#next').attr('disabled', false);

        list = response["data"];
        console.log(JSON.stringify(list));
        showList(list);
    })
    .fail(function (error) {
        console.error('AJAX error');
    })
    .complete(function (data) {
        console.log('AJAX completed');
    });
}



$(document).ready(function() {
	$('#prev').attr('disabled', true);
	$('#next').attr('disabled', true);

	$('#search-video').submit( function(e) {
		e.preventDefault();
		query = $('#query').val();
		getVideo(query, '');
	});

	$('#prev').click( function() {
		getVideo(query, prevPageToken);
	});

	$('#next').click( function() {
		getVideo(query, nextPageToken);
	});
})